import 'package:flutter/material.dart';

import 'home.dart';

class WorkOutDetailView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFeeeeee),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          "Front Squat",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w900,
            letterSpacing: 0.7,
            fontSize: 24,
          ),
        ),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        children: <Widget>[
          Image.asset(
            "assets/images/front-squat.png",
            fit: BoxFit.contain,
            height: 320,
          ),
          SizedBox(height: 16),
          Text(
            "Exercie",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 8),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.grey.withOpacity(0.3),
                      child: Icon(
                        Icons.directions_walk,
                        color: Colors.black54,
                      ),
                    ),
                    SizedBox(width: 8),
                    Column(
                      children: <Widget>[
                        Text(
                          "60kg",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 2),
                        Text(
                          "Weight",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.grey.withOpacity(0.3),
                      child: Icon(
                        Icons.close,
                        color: Colors.black54,
                      ),
                    ),
                    SizedBox(width: 8),
                    Column(
                      children: <Widget>[
                        Text(
                          "10",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 2),
                        Text(
                          "Reps",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.grey.withOpacity(0.3),
                      child: Icon(
                        Icons.replay,
                        color: Colors.black54,
                      ),
                    ),
                    SizedBox(width: 8),
                    Column(
                      children: <Widget>[
                        Text(
                          "10",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 2),
                        Text(
                          "Sets",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 32),
          Text(
            "Muscles",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 8),
          Wrap(
            spacing: 12,
            runSpacing: 8,
            children: ["Chest", "Arms", "Biceps"]
                .map((muscle) => Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 5,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.4),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Text(muscle),
                    ))
                .toList(),
          ),
          SizedBox(height: 32),
          Text(
            "History",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 8),
          Row(
            children: <Widget>[
              Expanded(
                child: HistoryItem(
                  number: 54,
                  title: "Completed Sets",
                  text: "2 per weeks",
                ),
              ),
              SizedBox(width: 16),
              Expanded(
                child: HistoryItem(
                  number: 55,
                  title: "Weight",
                  text: "-12%",
                ),
              ),
            ],
          ),
          SizedBox(height: 32),
        ],
      ),
    );
  }
}

class HistoryItem extends StatelessWidget {
  final int number;
  final String title;
  final String text;

  const HistoryItem({
    Key key,
    this.number,
    this.title,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          height: 56,
          width: 48,
          decoration: BoxDecoration(
            color: Color(0XFFdddddd),
            borderRadius: BorderRadius.circular(6),
          ),
          child: Center(
            child: Text(
              number.toString(),
              style: TextStyle(
                fontSize: 16,
                color: Color(0XFFaaaaaa),
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        SizedBox(width: 12),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 4),
              Text(text),
            ],
          ),
        ),
      ],
    );
  }
}
