import 'package:fitness/workout_detail.dart';
import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          bottom: TabBar(
            labelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 4,
            indicatorColor: Colors.black,
            tabs: <Widget>[
              Tab(text: "Today Workout"),
              Tab(text: "Week"),
              Tab(text: "Month"),
            ],
          ),
          title: Text(
            'Good morning!',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w900,
              letterSpacing: 0.7,
              fontSize: 24,
            ),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.account_circle,
                size: 32,
                color: Colors.black,
              ),
            )
          ],
        ),
        body: TabBarView(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 32,
                right: 24,
                left: 24,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "5 workouts",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    ListView.separated(
                      padding: const EdgeInsets.only(top: 24),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => WorkOutDetailView(),
                            ));
                          },
                          child: WorkOutItem(
                            number: index + 1,
                            title: "Front Squat",
                            items: ["Chest", "Thigh", "Arms"],
                          ),
                        );
                      },
                      separatorBuilder: (_, __) => SizedBox(height: 24),
                      itemCount: 10,
                    ),
                  ],
                ),
              ),
            ),
            Center(child: Text("Week")),
            Center(child: Text("Month")),
          ],
        ),
      ),
    );
  }
}

class WorkOutItem extends StatelessWidget {
  final int number;
  final String title;
  final List<String> items;

  const WorkOutItem({
    Key key,
    this.number,
    this.title,
    this.items,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          height: 64,
          width: 56,
          decoration: BoxDecoration(
            color: Color(0XFFdddddd),
            borderRadius: BorderRadius.circular(6),
          ),
          child: Center(
            child: Text(
              number.toString(),
              style: TextStyle(
                fontSize: 20,
                color: Color(0XFFaaaaaa),
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        SizedBox(width: 24),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Row(
                children: items
                    .map((item) => Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: Text(item),
                        ))
                    .toList(),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
